import unittest
from unittest import mock
from calc import calculator

obj=calculator()
read=open("/Users/gunasekhar/PycharmProjects/pythonProject/venv/resources/input_file.txt")
input_list=read.readlines()

class MyTestCase(unittest.TestCase):
    def test_add1(self):
        args = input_list[0].split(",")
        self.assertEqual(obj.add(int(args[0]),int(args[1])),int(args[2].strip()))
    def test_add2(self):
        args = input_list[1].split(",")
        self.assertEqual(obj.add(args[0], int(args[1])), args[2].strip())
    @mock.patch("calc.randint",return_value=5)
    def test_add3(self,mock_randint):
        args = input_list[2].split(",")
        assert obj.add(args[0].strip(),int(args[1])) == int(args[2].strip())
    def test_substract1(self):
        args = input_list[3].split(",")
        self.assertEqual(obj.substract(int(args[0]),int(args[1])),int(args[2].strip()))
    def test_substract2(self):
        args = input_list[4].split(",")
        self.assertEqual(obj.substract(args[0],int(args[1])),args[2].strip())
    @mock.patch("calc.randint",return_value=5)
    def test_substract3(self,mock_randint):
        args = input_list[5].split(",")
        assert obj.substract(args[0],int(args[1])) == int(args[2].strip())
    def test_multiply1(self):
        args = input_list[6].split(",")
        self.assertEqual(obj.multiply(int(args[0]),int(args[1])),int(args[2]))
    def test_multiply2(self):
        args = input_list[7].split(",")
        self.assertEqual(obj.multiply(args[0],int(args[1])),args[2].strip())
    @mock.patch("calc.randint",return_value=5)
    def test_multiply3(self,mock_randint):
        args = input_list[8].split(",")
        assert obj.multiply(args[0],int(args[1])) == int(args[2])
    def test_divide1(self):
        args = input_list[9].split(",")
        self.assertEqual(obj.divide(int(args[0]),int(args[1])),int(args[2]))
    def test_divide2(self):
        args = input_list[10].split(",")
        self.assertEqual(obj.divide(int(args[0]),int(args[1])),args[2].strip())
    @mock.patch("calc.randint",return_value=5)
    def test_divide3(self,mock_randint):
        args = input_list[11].split(",")
        self.assertEqual(obj.divide(args[0],int(args[1])),int(args[2]))
    def test_divide4(self):
        args = input_list[12].split(",")
        assert obj.divide(args[0],int(args[1])) == args[2].strip()

if __name__=="__main__":
    unittest.main()

